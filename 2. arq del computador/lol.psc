Algoritmo detect_num_primo
	
	//Definicion de datos
	Definir  j, operacion, a Como Entero
	a = 9
	cantDibisible = 0
	
	//solicitud de datos al usuario
	escribir "ingrese un n�mero: "
	leer j
	
	//filtro evitando 1 � 0
	si j > 2 Entonces
		
		//ejecucion de validaci�n
		Para i<-2 hasta a Hacer
			operacion = j%i			
			Escribir j " % " i " = " operacion
			si operacion = 0 Entonces
				cantDibisible = cantDibisible + 1
			FinSi
			Escribir "veces de divisibildad " cantDibisible
		FinPara
		
		//diagn�stico
		si cantDibisible > 1 Entonces
			Escribir "El n�mero " j " no es primo"
		SiNo
			si cantDibisible < 2 Entonces
				Escribir "El n�mero " j " es primo"
			FinSi
		FinSi
	SiNo		
		Escribir "El n�mero ingresado fu� " j ", no puede ser 2 ni inferior"
	FinSi
	
FinAlgoritmo
